﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HangMan
{
    public static class CLI_Helper
    {
        public static string UserInput { get; set; }

        public static bool IsValidUserEntry_LetterGuess(string message)
        {
            string userInput = String.Empty;
            int attemptCount = 0;
            do
            {
                if (attemptCount > 0)
                {
                    Console.WriteLine("Invalid input, please don't enter an empty string.");
                }
                Console.Write(message + " ");
                userInput = Console.ReadLine();
                attemptCount++;
            } while (userInput.Equals(String.Empty));
            UserInput = userInput;
            return true;
        }
        public static bool IsValidUserEntry_QuitCheck(string message)
        {
            string userInput = String.Empty;
            int attemptCount = 0;
            do
            {
                if (attemptCount > 0)
                {
                    Console.WriteLine("Invalid input, please enter Y or N only.");
                }
                Console.Write(message + " ");
                userInput = Console.ReadLine().ToUpper();
                attemptCount++;
            } while (!(userInput.Equals("Y") || userInput.Equals("N")));
            UserInput = userInput;
            return true;
        }
        public static bool IsValidUserEntry_LevelInt(string message)
        {
            string userInput = String.Empty;
            int attemptCount = 0;
            int valueRead = -1;
            do
            {
                if (attemptCount > 0)
                {
                    Console.WriteLine("Invalid input, please enter a number between 1 and 4, inclusive.");
                }
                Console.Write(message + " ");
                userInput = Console.ReadLine();
                attemptCount++;
            } while ((!int.TryParse(userInput, out valueRead)) && (userInput.Equals(String.Empty)));
            UserInput = userInput;
            return true;
        }



    }
}
