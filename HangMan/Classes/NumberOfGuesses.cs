﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HangMan.Classes
{
    public class NumberOfGuesses
    {
        private int initialGuesses = -1;

        public int NumberOfChances(int level)
        {
            switch (level)
            {
                case 1:
                    initialGuesses = 9;
                    break;

                case 2:
                    initialGuesses = 7;
                    break;

                case 3:
                    initialGuesses = 5;
                    break;

                case 4:
                    initialGuesses = 2;
                    break;

                default:
                    initialGuesses = 7;
                    break;
            }
            return initialGuesses;

        }

        public void DisplayRemainingGuesses(int chancesLeft, List<char> guessedLetters)
        {
            Console.WriteLine("Number of guesses remaining");
            Console.WriteLine("---------------------------");

            Console.ForegroundColor = ConsoleColor.Red;

            for (int i = 0; i < chancesLeft; i++)
            {
                Console.Write("X ".PadLeft(3));
            }
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("---------------------------");
            Console.WriteLine();
            Console.WriteLine("Guessed Letters: ");
            Console.WriteLine("---------------------------");
            Console.ForegroundColor = ConsoleColor.Yellow;

            foreach (Char letter in guessedLetters)
            {
                Console.Write((" " + letter));
            }
            Console.WriteLine();
            Console.ResetColor();
            Console.WriteLine("---------------------------");



        }

    }
}
