﻿using System;
using System.Collections.Generic;

namespace HangMan.Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            HangManCLI hangMan = new HangManCLI();
            hangMan.RunGame();
        }
    }
}
