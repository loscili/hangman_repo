﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HangMan.Classes
{
    public class HangManCLI
    {
        private int difficulty = 0;
        private int totalGuesses = -1;

        private bool isWordFound = false;
        private bool playAgain = true;
        private bool alreadyGuessed = false;
        private bool validUserInput = false;

        private char guessedLetter;
        private string wordToFind = String.Empty;
        private string wordAsDashesInitial = String.Empty;
        private string playerBuiltWord = String.Empty;
        private string quitCheck = String.Empty;

        private HashSet<char> guessedLettersHash = new HashSet<char>();
        private List<char> guessedLettersList = new List<char>();
        private RandomWordGenerator wordList = new RandomWordGenerator();
        private DisplayHiddenWord progressTracker = new DisplayHiddenWord();
        private SelectDifficulty selectDifficulty = new SelectDifficulty();
        private NumberOfGuesses numberOfGuesses = new NumberOfGuesses();
        private WinLossConfirmation winLossConfirmation = new WinLossConfirmation();

        public void RunGame()
        {
            Logo logo = new Logo();
            //logo.DisplayLogo();
            logo.AnimateDisplay();
            Console.Write("\n\nPress enter to enter: ");
            Console.ReadLine();
            Console.Clear();
            while (playAgain)
            {
                // Console asks user to enter a level of difficulty:
                Console.Clear();
                difficulty = selectDifficulty.LevelSelect();

                // Level : Chance pairs -- 1 : 9, 2 : 7, 3 : 5, 4 : 2
                totalGuesses = numberOfGuesses.NumberOfChances(difficulty);

                // select a word to solve for from our dictionary of words, or word file ;-)
                wordToFind = wordList.NextWord().ToUpper();
                // send wordToFind to the redrawnWord method to display "- - - - - -"
                wordAsDashesInitial = wordList.WordAsDashes;
                playerBuiltWord = wordAsDashesInitial;
                
                // this while loop needs to be broken manually for display purposes
                while (!isWordFound || totalGuesses > -1)
                {
                    Console.Clear();
                    Console.WriteLine();

                    // Display the number of chances left as Xs
                    // Display the incorrectly guessed letters, too (at level 4, do not display?)
                    numberOfGuesses.DisplayRemainingGuesses(totalGuesses, guessedLettersList);
                    Console.ResetColor();
                    Console.WriteLine();

                    // Display redrawnWord
                    // Displays either "- - - - -" for an unsolved word or "- - A - T" for a partially solved word
                    playerBuiltWord = progressTracker.DisplayProgress(wordToFind, guessedLetter, playerBuiltWord);

                    // if the word is fully revealed or guesses remaining == 0, we will break the && while loop
                    if (playerBuiltWord.Equals(wordToFind)) {
                        isWordFound = true;
                        break;
                    }
                    if (totalGuesses == 0)
                    {
                        break;
                    }
                    // Ask the user for their guessed letter
                    // Check if that letter {it's a <char>) has been guessed against a hashSet of guessed letters
                    do
                    {
                        while (!validUserInput)
                        {
                            validUserInput = CLI_Helper.IsValidUserEntry_LetterGuess("Guess a letter: ");
                        }
                        guessedLetter = CLI_Helper.UserInput.ToUpper()[0];
                        validUserInput = false;
                        if (guessedLettersHash.Add(guessedLetter))
                        {
                            alreadyGuessed = false;
                        }
                        else
                        {
                            Console.WriteLine($"You already guessed {guessedLetter}.");
                            alreadyGuessed = true;
                        }
                    } while (alreadyGuessed);

                    // Check if the guessed letter is part of the hidden word
                    bool correctGuess = (wordToFind.Contains(guessedLetter));
                    // True: update redrawnWord to include the new character where it occurs
                    if (correctGuess)
                    {
                        Console.WriteLine("You found one!\nPress enter to continue.");
                        Console.ReadLine();
                    }
                    // False: Decrement the totalGuesses & Add that letter to a hashSet of guessed letters
                    if (!correctGuess)
                    {
                        // If totalGuesses == 0, game ends as loss
                        // else, keep playing with one fewer chances to guess
                        totalGuesses--;
                        guessedLettersList.Add(guessedLetter);
                        guessedLettersList.Sort();
                        Console.WriteLine("oops...\nPress enter to continue.");
                        Console.ReadLine();
                    }
                }
                // Display win / loss confirmation
                winLossConfirmation.DisplayWinLossOutcome(isWordFound, wordToFind);

                // Ask to Continu Playing
                validUserInput = false;
                while (!validUserInput)
                {
                    validUserInput = CLI_Helper.IsValidUserEntry_QuitCheck("Shall we play again (Y/N)? ");
                }
                quitCheck = CLI_Helper.UserInput;
                validUserInput = false;
                playAgain = (quitCheck.Equals("N")) ? QuitGame() : InitializeGame();
            }
        }

        // re-init the hangman variables to continue playing
        // we will reset the level difficulty too
        private bool InitializeGame()
        {
            totalGuesses = -1;
            isWordFound = false;
            guessedLettersHash.Clear();
            guessedLettersList.Clear();
            guessedLetter = '-';
            quitCheck = String.Empty;
            return true;
        }

        // return a false bool to end the game
        private bool QuitGame()
        {
            Console.WriteLine();
            Console.Write("Thank you for playing!\nPress ENTER to exit. ");
            Console.ReadLine();
            return false;
        }
    }
}

