﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HangMan.Classes
{
    public class DisplayHiddenWord
    {
        // This needs to be the only method for displaying the progress of the revealing word
        // Need to check the guessed letter against the letters in the word to be found?
        // 
        public string DisplayProgress(string wordToFind, char guessedLetter, string progressionWord)
        {
            string progressDisplayWord = String.Empty;
            char[] dashesAndLetters = progressionWord.ToCharArray();
            char compareLetter = ' ';

            for (int i = 0; i < wordToFind.Length; i++)
            {
                compareLetter = wordToFind[i];
                if (compareLetter.Equals(guessedLetter))
                {
                    dashesAndLetters[i] = guessedLetter;
                }
                progressDisplayWord += dashesAndLetters[i] + " ";
            }
            Console.WriteLine("Word to Guess:");
            Console.WriteLine("---------------------------");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(progressDisplayWord);
            Console.ResetColor();
            Console.WriteLine("---------------------------");

            //parse out the spaces to return the basic string to compare against the wordToFind
            string[] progressWordNoSpaces = progressDisplayWord.Split(" ");
            return String.Join("", progressWordNoSpaces);
        }


    }
}
