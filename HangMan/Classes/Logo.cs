﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;

namespace HangMan.Classes
{
    public class Logo
    {
        private string[] logoArray = new string[20]; // I know the number of lines in the Logo
        private int logoArrayIndex = 0;

        public Logo()
        {
            StreamReader sr;
            string workingDirectory = Environment.CurrentDirectory;
            string fileName = "Logo_HangMan.txt";
            string fullPath = Path.Combine(workingDirectory, fileName);
            try
            {
                using (sr = new StreamReader(fullPath))
                {
                    int arrayIndex = 0;

                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        logoArray[++arrayIndex] = line;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error reading file");
                Console.WriteLine(e);
            }
        }
        // Display the whole logo all at once
        public bool DisplayLogo()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            foreach (string line in logoArray)
            {
                Console.WriteLine(line);
            }
            Console.ResetColor();
            return true;
        }
        
        // The word MAN falls out of the word HANG
        public bool AnimateDisplay()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            // i need to draw the word MAN from bottom most row to top most row. 
            // Increasing the number of rows drawn by 1 on each screen redraw.
            // in animate first 20, then 19, 20, then 18, 19, 20, etc. til 10 - 20;
            int breakLoopIndex = 10;
            int stopAtIndex = 20; // this is the bottom line of MAN
            int drawingStartIndex = 20; // this is the index at which we will start drawing
            int startAtIndex = 20; // this will be the top line of MAN
            int numberOfLinesToDisplay = startAtIndex - stopAtIndex;
            int numberOfLinesAlreadyDisplayed = 0; // store the number of lines displayed for re-drawn screen
            int waitTime = 150;

            while (startAtIndex >= breakLoopIndex)
            {
                Console.Clear();
                DisplayPartialLogo(10);
                numberOfLinesToDisplay = numberOfLinesAlreadyDisplayed;
                for (int i = numberOfLinesToDisplay; numberOfLinesToDisplay > 0; numberOfLinesToDisplay--)
                {
                    Console.WriteLine(logoArray[drawingStartIndex]);
                    drawingStartIndex++; // increment to get to the next LOWEST line of MAN.
                }
                numberOfLinesAlreadyDisplayed++; // on the next iteration of the while loop we will draw one more line of MAN
                startAtIndex = stopAtIndex - numberOfLinesAlreadyDisplayed; // "decrement" start index from which the MAN will begin to be drawn
                drawingStartIndex = startAtIndex;
                Thread.Sleep(waitTime);
            }
            Console.ResetColor();
            return true;
        }

        public bool DisplayPartialLogo(int indexToReadTo)
        {
            for (int i = 0; i < indexToReadTo; i++)
            {
                Console.WriteLine(logoArray[i]);
            }
            return true;
        }

        public int ReturnSizeOfArray()
        {
            return logoArray.Length;
        }
    }
}
