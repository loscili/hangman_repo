﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HangMan.Classes
{
    public static class Lexicon
    {
        private static List<string> lexicon;
        private static Random r;

    static Lexicon()
        {
            lexicon = new List<string>();

            StreamReader sr;
            string workingDirectory = Environment.CurrentDirectory;
            string fileName = "LargeLexicon.txt";
            //string fileName = "SimpleLexicon.txt";
            string fullPath = Path.Combine(workingDirectory, fileName);
            try
            {
                using (sr = new StreamReader(fullPath))
                {
                    while (!sr.EndOfStream)
                    {
                        string word = sr.ReadLine();
                        lexicon.Add(word);
                    }
                }
                r = new Random();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error reading lexicon file");
                Console.WriteLine(e);
            }
        }

        public static string GetWordFromLexicon(int index)
        {
            string randomWord = lexicon[index];
            lexicon.RemoveAt(index);
            return randomWord;
        }

        public static int GetSizeOfLexicon()
        {
            return lexicon.Count;
        }
    }
}
