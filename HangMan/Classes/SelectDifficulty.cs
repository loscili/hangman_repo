﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HangMan.Classes
{
    public class SelectDifficulty
    {
        private bool levelEntry = false;
        private int level = -1;
        private bool validInput = false;
        private bool existingLevel = false;

        public int LevelSelect()
        {
            Console.WriteLine("Levels of difficulty:");
            Console.WriteLine();
            Console.WriteLine("Easy -- 1");
            Console.WriteLine("Medium -- 2");
            Console.WriteLine("Hard -- 3");
            Console.WriteLine("Difficult -- 4");
            Console.WriteLine();

            while (!validInput && !existingLevel)
            {
                validInput = CLI_Helper.IsValidUserEntry_LevelInt("Select a level: ");
                int userLevel = int.Parse(CLI_Helper.UserInput);
                if (validInput)
                {
                    if (userLevel > 0 && userLevel < 5)
                    {
                        level = userLevel;
                        existingLevel = true;
                    }
                    else
                    {
                        validInput = false;
                        Console.WriteLine("Only values from 1 to 4 please.");
                    }
                }
            }
            // re-initialie in case we want to play again.
            validInput = false;
            existingLevel = false;
            return level;
        }
    }
}
