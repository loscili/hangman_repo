﻿using HangMan.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace HangManTests
{
    [TestClass]
    public class LogoTest
    {
        [TestMethod]
        public void CheckLogoArrayConstructor()
        {
            Logo logo = new Logo();
            Assert.AreEqual(19, logo.ReturnSizeOfArray());
        }

        [TestMethod]
        public void CheckDrawLogo()
        {
            Logo logo = new Logo();
            Assert.AreEqual(true, logo.DisplayLogo());
        }
        [TestMethod]
        public void CheckAnimateLogo()
        {
            Logo logo = new Logo();
            Assert.AreEqual(true, logo.AnimateDisplay());
        }


    }
}
