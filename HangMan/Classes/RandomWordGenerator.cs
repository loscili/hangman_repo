﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HangMan.Classes
{
    public class RandomWordGenerator
    {
        public string WordToGuess { get; private set; }
        public string WordAsDashes { get; private set; }

        private Random rand = new Random();
        private int randomIndex = 0;
        private HashSet<int> revealedWordKeys = new HashSet<int>();

        public RandomWordGenerator()
        {
        }

        public string NextWord()
        {
            //TODO track guessed word indexes to avoid repetitions, or Remove word from the dictionary 
            // TODO Need a check against an empty dictionary to avoid exception errors
            randomIndex = rand.Next(0, Lexicon.GetSizeOfLexicon() - 1);
            WordToGuess = Lexicon.GetWordFromLexicon(randomIndex);
            WordAsDashes = ConvertToDashes(WordToGuess);
            return WordToGuess;
        }

        //initially convert the word to find as all dashes
        public string ConvertToDashes(string word)
        {
            string convertingWord = "";
            for (int i = 0; i < WordToGuess.Length; i++)
            {
                convertingWord += "-";
            }
            return convertingWord;
        }
    }
}
