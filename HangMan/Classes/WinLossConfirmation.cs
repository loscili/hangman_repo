﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HangMan.Classes
{
    class WinLossConfirmation
    {
        public void DisplayWinLossOutcome(bool win, string wordToHaveBeenFound)
        {
            if (win)
            {
                Console.WriteLine("YOU ARE A WINNER!!");
            }
            else
            {
                Console.WriteLine("Sorry, out of chances.");
                Console.WriteLine();
                Console.WriteLine("Word to find was:");
                Console.WriteLine("---------------------------");
                Console.ForegroundColor = ConsoleColor.Blue;
                for (int i = 0; i < wordToHaveBeenFound.Length; i++)
                {
                    Console.Write(wordToHaveBeenFound[i] + " ");
                }
                Console.WriteLine();
                Console.ResetColor();
                Console.WriteLine("---------------------------");
                Console.WriteLine();
            }
        }
    }
}
